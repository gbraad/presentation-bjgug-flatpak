# Flatpak

### Flatpak: the road to 'CI/CD' for desktop applications?
Gerard Braad

<span class="lightblue">me</span><span class="white">@gbraad</span><span class="orange">.nl</span>


## Who am I

  * <span class="orange">F/OSS</span>
  * <span class="red">OpenStack</span>
  * OpenShift / Kubernetes
  * <span class="lightblue">Fedora</span> Project  
  * Tinkerer
  * ...


## Software distribution model

  * packages


## Other platforms

  * Store model


## Who publishes applications?


## Packagers do a good job


## But they do not know everything

  * ariya / phantomjs

```  
QXcbConnection: Could not connect to display 
PhantomJS has crashed. Please read the bug reporting guide at
<http://phantomjs.org/bug-reporting.html> and file a bug report.
Aborted
```

[#14240](https://github.com/ariya/phantomjs/issues/14240)


## Bug report

![](img/report.png)


## Answer

![](img/answer.png)


## Issues can be subtle

  * compilation happens against a different set of libraries


## Flatpak

  * born under the GNOME umbrella
  * safe environment for running applications


## Flatpak

  * "build once, run everywhere"


## Flatpak

![](img/flatpak.png)


## Flatpak

  * application developers are in control of the release cycle


## Flatpak

  * "Docker for desktop apps"


## Flatpak

```
$ dnf install -y flatpak
```


## Add remotes

```
$ wget https://sdk.gnome.org/keys/gnome-sdk.gpg
$ flatpak remote-add --gpg-import=gnome-sdk.gpg gnome https://sdk.gnome.org/repo/
$ flatpak remote-add --gpg-import=gnome-sdk.gpg gnome-apps https://sdk.gnome.org/repo-apps/
```


## Flatpak

  * runtime


## Flatpak

  * runtime
  * application


## Install runtime

```
$ flatpak install gnome org.gnome.Platform 3.20
```

    9 delta parts, 71 loose fetched; 167139 KiB transferred in 130 seconds                                   
    Installing related: org.gnome.Platform.Locale
    
    2 metadata, 0 content objects fetched; 13 KiB transferred in 2 seconds                                   


## Install application

```
$ flatpak install gnome-apps org.gnome.gedit stable
```

    1 delta parts, 1 loose fetched; 1725 KiB transferred in 11 seconds                                       
    Installing related: org.gnome.gedit.Locale
    
    2 metadata, 0 content objects fetched; 6 KiB transferred in 4 seconds               


## Run application

```
$ flatpak run org.gnome.gedit
```

    ** (gedit:2): WARNING **: Couldn't connect to accessibility bus: Failed to connect to socket /tmp/dbus-ZoHXu4deCC: Connection refused
    Gtk-Message: Failed to load module "pk-gtk-module"
    Gtk-Message: Failed to load module "pk-gtk-module"
    
    (gedit:2): Gtk-WARNING **: Could not load a pixbuf from icon theme.
    This may indicate that pixbuf loaders or the mime database could not be found.


## LibreOffice

```
$ wget http://download.documentfoundation.org/libreoffice/flatpak/latest/LibreOffice.flatpak
$ flatpak install --user gnome org.gnome.Platform.Locale 3.20
$ flatpak install --user --bundle LibreOffice.flatpak
```

```
$ flatpak run org.libreoffice.LibreOffice
```


## LibreOffice

![](img/libreoffice.png)


## Anatomy

```
[Application]
name=org.gnome.gedit
runtime=org.gnome.Platform/x86_64/3.20
sdk=org.gnome.Sdk/x86_64/3.20
command=gedit
```


## Install SDK

```
$ flatpak install gnome org.gnome.Sdk 3.20
```

    Receiving delta parts: 17/17 721.6 kB/s 33.2 MB/184.0 MB 3 minutes 28 seconds remaining                                                                                                                                                       Receiving delta parts: 17/17 708.0 kB/s 33.3 MB/184.0 MB 3 minutes 32 seconds remaining                                                                                                                                                       17 delta parts, 16 loose fetched; 33118 KiB transferred in 55 seconds                                                                                                                                                                         
    Installing related: org.gnome.Sdk.Locale
    
    2 metadata, 0 content objects fetched; 13 KiB transferred in 2 seconds


## Enter the sandbox

```
$ flatpak run --devel --command=bash org.gnome.gedit
```


## Home

```
bash-4.3$ ls -al
```

    total 172
    drwx------ 22 gbraad    gbraad     4096 Jul 28 13:36 .
    drwxr-xr-x  3 nfsnobody nfsnobody  4096 Jul 28  2016 ..
    drwx------  3 gbraad    gbraad     4096 Jul 28 11:44 .ansible
    -rw-------  1 gbraad    gbraad     1301 Jul 28 11:54 .bash_history
    -rw-r--r--  1 gbraad    gbraad       18 May 17 22:22 .bash_logout
    -rw-r--r--  1 gbraad    gbraad      193 May 17 22:22 .bash_profile
    -rw-r--r--  1 gbraad    gbraad      231 May 17 22:22 .bashrc
    drwx------ 16 gbraad    gbraad     4096 Jul 28 11:26 .cache
    drwxr-xr-x 18 gbraad    gbraad     4096 Jul 28 13:36 .config
    drwxr-xr-x  2 gbraad    gbraad     4096 Jul 28 13:36 Desktop
    drwxr-xr-x  2 gbraad    gbraad     4096 Jul 28 10:38 Documents
    [...]


## Root

```
bash-4.3$ ls -al /
```

    total 44
    drwxr-xr-x  17 gbraad    gbraad      420 Jul 28 13:37 .
    drwxr-xr-x  17 gbraad    gbraad      420 Jul 28 13:37 ..
    drwxrwxr-x   6 nfsnobody nfsnobody  4096 Jul 28 11:07 app
    lrwxrwxrwx   1 gbraad    gbraad        7 Jul 28 13:37 bin -> usr/bin
    drwxr-xr-x   4 gbraad    gbraad      300 Jul 28 13:37 dev
    drwxr-xr-x  24 gbraad    gbraad     1140 Jul 28 13:37 etc
    drwxr-xr-x   3 nfsnobody nfsnobody  4096 Jul 28  2016 home
    lrwxrwxrwx   1 gbraad    gbraad        7 Jul 28 13:37 lib -> usr/lib
    lrwxrwxrwx   1 gbraad    gbraad        9 Jul 28 13:37 lib64 -> usr/lib64
    drwx------   2 nfsnobody nfsnobody 16384 Jun 15 00:25 lost+found
    drwxr-xr-x   2 nfsnobody nfsnobody  4096 Feb  4 06:10 media
    drwxr-xr-x   2 nfsnobody nfsnobody  4096 Feb  4 06:10 mnt
    drwxr-xr-x   2 nfsnobody nfsnobody  4096 Feb  4 06:10 opt
    dr-xr-xr-x 243 nfsnobody nfsnobody     0 Jul 28 13:37 proc
    drwxr-xr-x   6 gbraad    gbraad      160 Jul 28 13:37 run
    lrwxrwxrwx   1 gbraad    gbraad        8 Jul 28 13:37 sbin -> usr/sbin
    drwxr-xr-x   2 nfsnobody nfsnobody  4096 Feb  4 06:10 srv
    drwxr-xr-x   7 gbraad    gbraad      140 Jul 28 13:37 sys
    drwxr-xr-x   3 gbraad    gbraad       60 Jul 28 13:37 tmp
    drwxrwxr-x  14 nfsnobody nfsnobody  4096 Jul 28 13:36 usr
    drwxr-xr-x   6 gbraad    gbraad      140 Jul 28 13:37 var


## App

```
bash-4.3$ ls -al /app
```

    total 28
    drwxrwxr-x  6 nfsnobody nfsnobody 4096 Jul 28 11:07 .
    drwxr-xr-x 17 gbraad    gbraad     420 Jul 28 13:37 ..
    drwxrwxr-x  2 nfsnobody nfsnobody 4096 Jan  1  1970 bin
    drwxrwxr-x  7 nfsnobody nfsnobody 4096 Jan  1  1970 lib
    drwxrwxr-x  3 nfsnobody nfsnobody 4096 Jan  1  1970 libexec
    -rwxrw-r--  2 nfsnobody nfsnobody 2223 Jan  1  1970 manifest.json
    -rw-r--r--  1 nfsnobody nfsnobody    0 Jul 28 11:07 .ref
    drwxrwxr-x 15 nfsnobody nfsnobody 4096 Jan  1  1970 share


## Libraries

```
bash-4.3$ ls -al /usr/lib/ |wc -l
```

    1287

```
bash-4.3$ exit
exit
[gbraad@yoga ~]$ ls -al /usr/lib/ |wc -l
```

    406


## Flatpak

  * runtime
  * application


## Flatpak

  * /usr
  * /app


## Install location

  * System wide  
    `/var/lib/flatpak/`
  * User  
    `~/.local/share/flatpak/`


## OSTree

  * "git-like" model for committing and downloading bootable filesystem trees
  * checksums individual files
  * content-addressed-object store


## OSTree

  * Unlike git
    * it "checks out" the files via hardlinks
    * immutable


## Example
```
$ mkdir repo
$ alias ost="ostree --repo=`pwd`/repo"

$ ost init
$ ls repo
config  objects  refs  remote-cache  tmp
```


## Example

```
$ mkdir tree
$ cd tree
$ echo "x" > 1
$ echo "y" > 2

$ ost commit --branch=my-branch \
    --subject="Initial commit" \
    --body="My commit"

$ ost ls my-branch
d00755 1002 1002      0 /
-00644 1002 1002      2 /1
-00644 1002 1002      2 /2
```


## Example

```
$ ost checkout my-branch
$ ls my-branch/
1  2
```


## Provides

  * something between 'package-based' and 'image-based' deployment


## Simple app

```
$ mkdir -p hello/files/bin
$ mkdir hello/export
```


`hello/files/bin/hello.sh`
```
#!/bin/sh
echo "Hello world, from a sandbox"
```


## Simple app (continued)

`hello/metadata`
```
[Application]
name=org.test.Hello
runtime=org.gnome.Platform/x86_64/3.20
command=hello.sh
```

```
$ flatpak build-export repo hello
```


## Simple app (install and run)
```
$ flatpak --user remote-add --no-gpg-verify tutorial-repo repo
$ flatpak --user install tutorial-repo org.test.Hello
```

```
$ flatpak run org.test.Hello
```

    Hello world, from a sandbox.



## Changes
```
$ flatpak --user update org.test.Hello
```


## Build process
```
$ wget https://download.gnome.org/sources/gnome-dictionary/3.20/gnome-dictionary-3.20.0.tar.xz
$ tar xvf gnome-dictionary-3.20.0.tar.xz
$ cd gnome-dictionary-3.20.0/
```

```
$ flatpak build ../dictionary ./configure --prefix=/app
$ flatpak build ../dictionary make
$ flatpak build ../dictionary make install
```


## Flatpak builder

  * more complex applications
  * additional dependencies
  * automate multi-step build process


## Build definition
```
{
  "app-id": "org.gnome.Dictionary",
  "runtime": "org.gnome.Platform",
  "runtime-version": "3.20",
  "sdk": "org.gnome.Sdk",
  "command": "gnome-dictionary",
  "finish-args": [ 
     "--socket=x11", 
     "--share=network"  
  ],
```


## Build definition (continued)

```
  "modules": [
    {
      "name": "gnome-dictionary",
      "sources": [
        {
          "type": "archive",
          "url": "https://download.gnome.org/sources/gnome-dictionary/3.20/gnome-dictionary-3.20.0.tar.xz",
          "sha256": "efb36377d46eff9291d3b8fec37baab2355f9dc8bc7edb791b6a625574716121"
        }
      ]
    }
  ]
}
```


## Build

```
$ flatpak-builder --repo=repo dictionary org.gnome.Dictionary.json
```


## Sandbox

  * sandboxing (bubblewrap)
    * namespaces


## Sandbox

  * No access to any host files
    * except the runtime, the app and ~/.var/app/$APPID
  * No access to the network
  * No access to any device nodes
    * `/dev/null`
  * No access to processes outside the sandbox
  * Limited syscalls
  * Limited access to the session D-Bus instance
  * No access to host services
    * X, system D-Bus, or PulseAudio


## Sandbox (continued)

```
  "finish-args": [ 
     "--socket=x11", 
     "--share=network"  
  ],
```

```
$ flatpak build-finish dictionary2 --socket=x11 --share=network --command=gnome-dictionary
```


## Result
```
[Application]
name=org.gnome.Dictionary
runtime=org.gnome.Platform/x86_64/3.20
sdk=org.gnome.Sdk/x86_64/3.20
command=gnome-dictionary

[Context]
shared=network;
sockets=x11;
```


## Permission can be overriden

```
$ flatpak run --filesystem=home --command=ls org.gnome.Dictionary ~/
```

Note: can also be enabled and disabled for an application


## What does it do

  * Create a new directory
  * Download and verify the Dictionary source code

  * Build and install the source code
    * using the SDK rather than the host system
  * Finish the build, by setting permissions
    * (in this case giving access to X and the network)
  * Export the resulting build to the repository


## Alternative runtimes

  * KDE

  * CentOS
  * openSUSE

  * Mono
  * ...


## Alternative runtimes

  * How much should be provided?


## Store model

  * Flathub
    * hosting
    * shared building


## How about tests?

  * Would be great if this can be 'automated'
    * GNOME Continuous


## GNOME Continuous

  * gnome-continuous build system
    * OpenEmbedded to build a base system


## Reproducible builds

  * why?


## Deterministic

  * security


## Flatpak for server?

  * desktop session


## Flatpak for server?

  * we already have Docker?


## Resources

  * http://flatpak.org
  * https://github.com/flatpak/flatpak/wiki/Examples
  * https://wiki.gnome.org/action/show/Projects/OSTree
  * https://wiki.gnome.org/Projects/GnomeContinuous
  * https://github.com/gbraad/docker-flatpak
  * https://gitlab.com/gbraad/flatpak-builder-gnome
  * https://gbraad.gitbooks.io/scratchpad/content/technology/flatpak.html
  * https://github.com/alexlarsson/nightly-build-apps


## Stay in touch

![](img/email.png)


## Stay in touch

![](img/wechat.jpg)


## Q & A
