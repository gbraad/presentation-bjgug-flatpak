# GNOME Release Party (Beijing)

## Flatpak, the road to CI/CD for desktop applications?
Gerard Braad

<span class="lightblue">me</span><span class="white">@gbraad</span><span class="orange">.nl</span>


  * [Slides](./slides.html) as HTML
  * [Slides](./slides.pdf) as PDF
  * [Handout](./handout.pdf)
